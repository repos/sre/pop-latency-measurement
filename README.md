# Wikimedia latency measurement

## Setting up a virtual environment

Inside the application directory execute the following to create a new virtual
environment:

```
$ python -m venv .venv
```

Activate your new virtual environment:

```
$ source .venv/bin/activate
```

Once you've activated your environment, install the dependecies:

```
$ pip install -r requirements.txt
```

Create an environment variable named `RIPE_API_TOKEN` to hold your token
generated from your RIPE account based on the [RIPE documentation](https://atlas.ripe.net/docs/apis/rest-api-manual/api_keys.html).

## Creating a measurement

To view all available measurement options type:

```
$ python main.py measurements --help
```

### Continent

The following command will create measurements for countries under the asia
with a tag named *asia2*. If the tag option is left out (it is best if you can
give a unique tag to all your measurements for easier retrieval), the default
tag set for that measurement is either continent name or country (if only
setting a measurement for a  single country).

```
$ python main.py measurements --dc esams eqsin drmrs --continent asia --tag asia2
```

The measurement will also be set to measure latency between the country and the
three data centers provided above (`esams`, `eqsin` and `drmrs`) and thus the
result will contain 3 separate measurements for comparison.

Not providing a `--dc` option will create measurement against all our data
centers.

### Country

The following command will create a measurement between the selected country
and data center. The tag for the measurement will be set to cyprus since no tag
option was provided in the tag.

```
$ python main.py measurements --dc esams eqsin drmrs --country cyprus
```

### Collecting Results

To view results of a particular ID execute:

```
$ python main.py results --id 41806230
```

To retrieve results based on a specific tag named *asiameasurement1*:

```
$ python main.py results --tag asiameasurement1
```

### Parsing results

To view the data center site latency order, pass a generated latency file to
`parser.py`:

```
$ python parser.py europeleo491427062022.csv
```
