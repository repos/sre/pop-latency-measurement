import statistics


def calculate_stats(data):
    """
    Returns a list containing the following values [mean, median, max, min]
    """

    rtts = [-1, -1, -1]
    try:
        rtts = [
            statistics.mean(data), statistics.median(data),
            max(data), min(data)
            ]
    except Exception:
        pass

    return rtts
