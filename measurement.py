from time import sleep
import requests as r

from datetime import datetime

from ripe.atlas.cousteau import Ping
from ripe.atlas.cousteau import AtlasSource
from ripe.atlas.cousteau import AtlasCreateRequest
from ripe.atlas.cousteau import AtlasResultsRequest
from ripe.atlas.cousteau import Probe
from ripe.atlas.cousteau.exceptions import APIResponseError

from ripe.atlas.sagan import PingResult

from utils import print_table
from utils import get_host_name
from utils import write_csv
from utils import countries

from stats import calculate_stats


def create_measurement(args):

    source = AtlasSource(
        type="country",
        value=args.countryCode,
        requested=100
    )

    for url in args.url:

        description = f"{args.countryName} : {args.countryCode} : \
            {args.continent} : Ping Latency from target url {url}"
        print(description)

        # measurement
        ping = Ping(
            description=description,
            af=4,
            target=url,
            is_public="false",
            tags=[
                i.lower() for i in [
                    args.tag or args.continent or args.countryCode
                    ]
                    ]  # Note, tags need to be in lower case
        )

        ping_request = AtlasCreateRequest(
            start_time=datetime.utcnow(),
            key=args.apiKey,
            measurements=[ping, ],
            sources=[source, ],
            is_oneoff=True
        )

        ok, response = ping_request.create()
        if not ok:
            print(response)


def init_measurements(args):

    countryList = countries(countries=args.country, continent=args.continent)
    measurementIDs = []
    print("Countries: ", len(countryList))
    print("Setting up measurements: Please hold as it will take sometime...")

    for i, c in enumerate(countryList):
        # sleep 5 minutes to allow Ripe to
        # complete concurrent measurements (a limit of )
        if i > 0 and i % 25 == 0:
            sleep(600)
        setattr(args, "countryCode", c[0])
        setattr(args, "countryName", c[1])
        create_measurement(args)

    # Prints if a single country measurement was set and not a continent
    if args.country:
        print("Measurement IDs for set Country: ", measurementIDs)

    # TODO: sort erroneous reporting


def query_ripe(params, nextURL=None, results=None):

    url = nextURL or \
        "https://atlas.ripe.net/api/v2/measurements/{}".format("my")
    if results:
        return results.append(r.get(url, params=params))
    return r.get(url, params=params)


def clean_data(data):
    """
    Avoids calculatestats from failing when None is passed
    """
    d = [0]*len(data)
    for i, j in enumerate(data):
        if j:
            d[i] = j
    return d


def get_ping_results(response):

    data = []
    stats = []
    for i, j in enumerate(response[1]):
        s = PingResult(j)
        if i == 0:
            # setting retries here
            probe = {}
            for i in range(5):
                try:
                    probe = Probe(id=s.probe_id)
                    break
                except APIResponseError:
                    continue
            # extract id , probe_id and destination name from first loop only
            data = [
                response[0], s.destination_name,
                get_host_name(s.destination_name), probe.country_code
                ]
        stats.extend(clean_data(
            [
                s.rtt_average, s.rtt_median, s.rtt_max, s.rtt_min
                ]
                )
                )
    return data + calculate_stats(stats)


def measurement_results(apiKey, measurementID, results=None):
    ok, raw_results = AtlasResultsRequest(
        msm_id=measurementID,
        key=apiKey).create()
    if ok:
        r = get_ping_results([measurementID, raw_results])
        if results:
            return results.append(r)
        return [r]

    return []


def generate_measurement_list(apiKey, results):

    data = []
    for i in results:
        d = measurement_results(apiKey, i["id"])[0]
        d.insert(1, i["description"])
        d.append(i["status"]["name"])
        data.append(d)
    return data


def list_measurements(apiKey):
    response = query_ripe({
        "key": apiKey, "type": "ping",
        "fields": "description,status"
        })
    if response.status_code == 200:
        data = generate_measurement_list(apiKey, response.json()["results"])
        header = [
            "ID", "Description", "Target IP", "Target Hostname", "Country",
            "rtt_Average", "rtt_Median", "rtt_Max", "rtt_min", "Status",
            "Probes Used"
            ]
        print_table(header, data)
    else:
        print("Failed to fetch data: ", response.status_code)


def filter_rtt(results):
    rtts = []
    for i in results:
        for j in i["result"]:
            if j.get("rtt"):
                rtts.append(j.get("rtt"))
    return calculate_stats(rtts)


def filter_description(description):
    description = description.split(":")
    k = description[3].split(".")
    return [k[1], description[0], description[1], description[2]]


def filter_results(results):
    endResults = []
    for i in results:
        k = [i["id"], i["target"], i["probes_scheduled"]]
        description = filter_description(i["description"])
        k = k + description + [i["result"]]
        endResults.append(k)
    return endResults


def call_url(url, params):
    x = None
    # Setting retries
    for i in range(5):
        try:
            x = r.get(url, params=params)
            break
        except Exception:
            continue
    return x


def get_ripe(params=None, nextPage=None):
    baseURL = "https://atlas.ripe.net/api/v2/measurements/{}"
    url = nextPage or baseURL.format(params.pop("path"))
    results = call_url(url, params)
    contents = []
    nextUrl = None
    if results.status_code == 200:
        content = filter_results(results.json()["results"])
        nextUrl = results.json()["next"]
        for i in content:
            # Using results url, get rtt results
            rttResults = call_url(i.pop(-1), params)
            if rttResults.status_code == 200:
                rtt = filter_rtt(rttResults.json())
                contents.append(i + rtt)
    return contents, nextUrl


def pull_measurements(args):
    results = get_ripe(
        params={
            "key": args.apiKey, "type": "ping",
            "id": args.id, "path": "my", "tags": args.tag
            }
        )
    metrics = results[0]
    # Collect extra pages if they exist
    while results[1]:
        results = get_ripe(nextPage=results[1])
        metrics = metrics + results[0]
    return metrics


def print_results(args):
    header = [
        "ID", "Target", "Probes Used", "Site",
        "Country", "Code", "Continent", "rtt_Average",
        "rtt_Median", "rtt_Max", "rtt_min"
        ]
    if args.id:
        print_table(header, pull_measurements(args))

    else:
        data = pull_measurements(args)
        data.insert(0, header)
        write_csv(args.tag, data)
