import argparse

from measurement import init_measurements
from measurement import print_results
from measurement import list_measurements

from utils import url_target_creator
from utils import fetch_token

apiToken = fetch_token()
dcs = ["eqiad", "ulsfo", "esams", "codfw", "eqsin", "drmrs", "magru"]


def measure(args):
    list_measurements(apiToken)


def results(args):
    setattr(args, "apiKey", apiToken)
    print_results(args)


def set_measurements(args):
    if not args.dc:
        setattr(args, "dc", dcs)
    setattr(args, "apiKey", apiToken)
    setattr(args, "url", url_target_creator(args.dc))
    init_measurements(args)


parser = argparse.ArgumentParser(
    description="""
    Creates and generates latency measurements
    on specified countries or continents
    against Wikimedias Datacenters around the world
    """
    )

# create subParsers
subParser = parser.add_subparsers()

# Parser for Measurements subcommand
parserMeasurements = subParser.add_parser("measurements")
parserMeasurements.set_defaults(func=set_measurements)
region = parserMeasurements.add_mutually_exclusive_group()
region.add_argument(
    "--country", type=str, nargs="*",
    help="Your targeted country code or country name"
    )
region.add_argument("--continent", type=str, help="Targeted continent")
parserMeasurements.add_argument("--dc", type=str, nargs="*", choices=dcs)
parserMeasurements.add_argument("--key", type=str, help="User's Ripe API Key")
parserMeasurements.add_argument(
    "--tag", type=str,
    help="""
    A tag to help you retrieve measurements
    in the absence of tag, country or continent is used
    """
    )
parserMeasurements.add_argument(
    "--probes", type=int,
    help="Number of probes to be used", default=50
    )

# Parser for listing measurements
listUserMeasurements = subParser.add_parser("list")
listUserMeasurements.set_defaults(func=measure)

# Parser for getting results
parserResults = subParser.add_parser("results")
parserResults.set_defaults(func=results)
parserResults.add_argument(
    "--id", help="Measurement ID to retrieve its results"
    )
parserResults.add_argument(
    "--tag", help="Measurement tag to retrieve its results"
    )
parserResults.add_argument(
    "--print", help="""
    Option enables results to be printed on screen.
    Best used when ID is selected as an option.
    """
    )

args = parser.parse_args()
args.func(args)
