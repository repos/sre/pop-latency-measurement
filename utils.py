from datetime import datetime
import os
import socket
import pycountry
import pycountry_convert as pc
import requests as r
import csv

from tabulate import tabulate
from ipaddress import ip_address


def print_table(header, data):
    print(tabulate(data, headers=header, tablefmt="fancy_grid"))


def write_csv(fileName, data):
    fileName = fileName + datetime.now().strftime("%M%H%d%m%Y") + ".csv"
    print("Writing data to file ", fileName)
    with open(fileName, mode='w') as resultFile:
        writer = csv.writer(resultFile)
        writer.writerows(data)


def fetch_token():
    apiToken = os.getenv("RIPE_API_TOKEN")

    if not apiToken:
        print("No environment variable named RIPE_API_TOKEN with your key set")
        return False

    return apiToken


def is_country(countries):

    """
    Checks validity of country code or country name provided.
    """
    countryList = []
    for c in countries:
        k = None
        if len(c) < 3:
            k = pycountry.countries.get(alpha_2=c)
        else:
            k = pycountry.countries.get(
                alpha_3=c) or pycountry.countries.get(name=c) \
                or pycountry.countries.get(common_name=c)
        if not k:
            return False
        counrtyCode = k.alpha_2
        continentName = pc.convert_continent_code_to_continent_name(
            pc.country_alpha2_to_continent_code(counrtyCode)
            )
        countryList.append([counrtyCode, k.name, continentName])
    return countryList


def is_continent(continent):
    continents = {
        'NA': 'NORTH AMERICA', 'SA': 'SOUTH AMERICA',
        'AS': 'ASIA', 'OC': 'OCEANIA', 'AF': 'AFRICA',
        'EU': 'EUROPE', "AUSTRALIA": "OCEANIA"
        }
    continent = continent.upper()
    continentCode = list(continents.keys())
    continentName = list(continents.values())

    if continent in continentCode+continentName:
        isValue = continents.get(continent)
        if isValue:
            return [continent, isValue]
        else:
            cCode = continentCode[continentName.index(continent)]
            return [cCode, continent]
    else:
        return []


def continent_countries(continent):

    site = "https://restcountries.com/v3.1/region/{}"
    continent = is_continent(continent)
    if not continent:
        return []
    site = site.format(continent[1])
    response = r.get(site)
    countries = []

    if response.status_code == 200:
        for i in response.json():
            countries.append([i["cca2"], i["name"]["common"], continent[1]])
    return countries


def countries(countries, continent):

    if countries:
        country = is_country(countries)
        if not country:
            print("Invalid country or country code provided")
            return []
        return country

    if continent:
        return continent_countries(continent)


def url_target_creator(dcs):

    if isinstance(dcs, str):
        dcs = [dcs, ]

    wikipediaURL = "text-lb.{}.wikimedia.org"
    urls = []

    for dc in dcs:
        urls.append(wikipediaURL.format(dc))

    return urls


def get_host_name(ip):
    try:
        ip_address(ip)
        return socket.gethostbyaddr(ip)[0]
    except ValueError:
        return ip
